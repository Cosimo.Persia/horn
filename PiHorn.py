# class Partial_Interpretation:
#     def __init__(self,pos,neg):
#         #sets
#         self.pos = pos
#         self.neg = neg
import copy
import decimal

def d(n):
    return decimal.Decimal(n) / decimal.Decimal(1)


class PiHorn:
    def __init__(self,MQ,variable_n,precision =2):
        self.variable_n = variable_n
        self.hypothesis = [] #list of pair
        self.S = [] #list of sets of antecedents
        self.MQ = MQ #function of 2 arguments
        self.precision = precision
        decimal.getcontext().prec=precision

    def process_counterexample(self,positive_var,possibility):
        possibility = d(possibility)
        necessity = d(1)-possibility
        alpha_hat = self.unit_propagation(positive_var,necessity)
        replaced = False
        for ix, (alpha,possibilityS) in enumerate(self.S):
            intersection = alpha.intersection(alpha_hat)
            if possibility == possibilityS and \
                len(intersection)>0 and (intersection < alpha) and\
                len(self.RHS(intersection,necessity))>0:

                self.S[ix][0] = intersection
                replaced = True
                break
        if not replaced:
            self.S.append([alpha_hat,possibility])
        # print(f'S {self.S}')

        self.hypothesis = []
        for idx, (alpha,rule_possibility) in enumerate(self.S):
            rule_necessity = d(1) - rule_possibility
            heads = self.RHS(alpha,rule_necessity)
            if len(heads) == 0:
                self.S.pop(idx)
            else:
                for h in heads:
                    self.hypothesis.append([alpha, {h},d(rule_necessity)])
        # print(f'hypo { self.hypothesis}')

    def unit_propagation(self, set_unit_clauses,necessity):
        rules = copy.deepcopy(self.hypothesis)
        forcefully_true = set()
        for c in set_unit_clauses:
            a = set()
            a.add(c)
            rules.append([set(),a,d(1)])
        no_propagation = False
        while not no_propagation:
            no_propagation = True
            for ix,(ant,cons,nec) in enumerate(rules):
                if nec >= necessity:
                    if len(ant) > 0:
                        for v in forcefully_true:
                            if v in ant:
                                rules[ix][0].remove(v)
                                no_propagation = False
                    if len(ant) == 0:
                        if len(cons) == 0:
                            raise Exception('cons is empty, incosistent rules')
                        if list(cons)[0] not in forcefully_true:
                            forcefully_true.add(list(cons)[0])
                            no_propagation = False

                # if len(ant) > 0:
                #     all_unit_clauses = False
        return forcefully_true

    def RHS(self,antecedent,rule_necessity):
        heads = set()
        candidates = set(range(self.variable_n))
        # candidates = self.unit_propagation(antecedent).difference(antecedent)
        candidates_list = list(candidates - (antecedent))
        if len(candidates_list) > 0:
            answer = self.MQ(antecedent,candidates_list)
            possibility = d(1) - rule_necessity
            for i,c in enumerate(candidates_list):
                if answer[i] <= possibility:
                    heads.add(c)
        return heads

