import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
from Horn import *
# Imports
import numpy as np
import timeit
from random import randint, choice
from copy import deepcopy, copy
from keras.models import Sequential
from keras.layers.core import Activation
from keras.layers.core import Dense
from keras.optimizers import SGD
from sklearn.model_selection import train_test_split
import seaborn as sn
import pandas as pd
import itertools
from tensorflow.keras.callbacks import EarlyStopping
import matplotlib.pyplot as plt
from chefboost import Chefboost as chef
from math import log
from scipy.special import comb
from skmultiflow.trees import HoeffdingTree
import random
from tensorflow.keras.utils import to_categorical

import tensorflow as tf
print("Num GPUs Available: ", len(tf.config.experimental.list_physical_devices('GPU')))

EPOCHS = 500
BATCH_SIZE = 64
SIZE_DIFFERENCES_TEST = 4000
ITERATIONS = 1
MAX_EQ_QUERIES = 100

def generate_target(n_variables, n_clauses):
    target = []
    variables = list(range(n_variables))
    for i in range(n_clauses):
        antecedent = set()
        antecedent.update(random.sample(variables,random.randint(2,n_variables//5)))

        consequent= set()
        consequent.update(random.sample(variables,1))

        target.append([antecedent,consequent])
    return target


def int2assignment(integer, n_variables ):
    """
    it takes as input  an integer between 0 and 2^n-1 and it outputs the
    assignment which corresponds to the binary encoding of the integer.
    """
    # Decimal to binar\betay number conversion
    assignment = [int(i) for i in list('{0:0b}'.format(integer))]
    while len(assignment) < n_variables:
        assignment = [0] + assignment
    for i in range(len(assignment)):
        if random.random() < 0.40:
            assignment[i] = -1
    return assignment

def uniform_distribution(n,n_variables):
    """
    it takes as input the number of variables and returns an iterator
    that generates number between 0 and 2^n-1, drawn according to
    an uniform probability distribution.
    """
    while True:
        yield int2assignment(randint(0,n),n_variables)


def get_eq_sample_size(ith_query,epsilon,delta):
    return int((1/epsilon)*(log(1/delta) + ith_query))+100

def trainingsize(n_variables):
    return 3000*n_variables

def satisfiable(target,part_int):
    for ant,cons in target:
        if part_int[list(cons)[0]] == 0:
            ant_satisfied = True
            for v in ant:
                if part_int[v] != 1:
                    ant_satisfied = False
                    break
            if ant_satisfied:
                return False
    return True


def create_classified_sample_randomly(target,n_assignments,n_variables,noise =0):
    dataset = np.empty((n_assignments,n_variables+1))
    distribution = uniform_distribution(pow(2,n_variables)-1,n_variables)
    for i in range(n_assignments):
        print('\r{}\{}   '.format(i,n_assignments-1),end='')
        assignment = next(distribution)
        l = (1 if satisfiable(target,assignment)
             else 0)
        if noise > 0 and noise < 40:
            if randint(1,100) <= noise:
                l = 1-l
        dataset[i,:] = np.array(assignment + [l])
        # dataset.append((assignment,l))
    return dataset

def nn(dim):
    model = Sequential()
    hidden_dim = dim // 2
    model.add(Dense(hidden_dim, input_dim=dim, activation='relu'))
    while hidden_dim > 31:
        hidden_dim = hidden_dim // 2
        model.add(Dense(hidden_dim, activation='relu'))
    model.add(Dense(1, activation='sigmoid'))
    # Compile model
    model.compile(loss='binary_crossentropy', optimizer = SGD(learning_rate = 0.01), metrics=['accuracy'])
    return model

def create_classified_sample_from_NN(NN,n_assignments,n_variables):
    dataset_t = np.empty((n_assignments,n_variables))
    distribution = uniform_distribution(pow(2,n_variables)-1,n_variables)
    for i in range(n_assignments):
#         print('{}\{}   '.format(i,n_assignments-1),end='\r')
        assignment = next(distribution)
        dataset_t[i,:] = np.array(assignment)
        # dataset_t.append(assignment)
    dx = to_categorical(dataset_t,num_classes=3)
    dx = dx.reshape(dx.shape[0],dx.shape[1]*3)
    ys = (NN.predict(dx, verbose = 0) > 0.5).astype("int32")
    dataset =np.empty((n_assignments,n_variables+1))
    for i in range(n_assignments):
        dataset[i, :] = np.concatenate((dataset_t[i,:], ys[i,:]),axis=0)
        # dataset.append((dataset_t[i],ys[i][0]))
    return dataset


def list_to_int(l):
    l.reverse()
    number = 0
    for pos, value in enumerate(l):
        number += pow(2, pos) * value
    return number



def inc_train_tree(clf,eq_queries,n_variables):
    tree = HoeffdingTree()
    start = timeit.default_timer()
    for i in range(eq_queries):
        sample = create_classified_sample_from_NN(clf,get_eq_sample_size(n_variables,0.05,0.05),n_variables)
        X = sample[:,:-1]
        y = sample[:,-1]
        p=tree.predict(X)
        if np.array_equal(y,p):
            break
        tree.partial_fit(X,y)
    return (tree,timeit.default_timer()-start)


#################################################################################
#################################################################################
#################################################################################
#################################################################################
#################################################################################
#################################################################################
def custom_EQ(hypothesis,ith, memory):
    clf = memory[0]
    n_variables = memory[1]
    bad_ne = memory[2]
    sample_size = get_eq_sample_size(ith, 0.05, 0.05)
    memory[3] += sample_size

    sample = create_classified_sample_from_NN(clf, sample_size,n_variables)
    #find negative counterexamples
    for row in sample:
        if  row[-1]  == 0 and satisfiable(hypothesis,row[:-1]):
            positive_var = set()
            for i in range(len(row[:-1])):
                if row[:-1][i] == 1:
                    positive_var.add(i)
            # memory[2].append(positive_var)
            return (False,positive_var)

    return (True,True)


# def custom_EQ2(memory):
#     clf = memory[0]
#     V = memory[1]
#     bad_ne = memory[2]
#     calls_to_NN = memory[3]
#
#     calls_to_NN += get_eq_sample_size(len(V),0.01,0.01)
#     memory[3] = calls_to_NN
#     dataset = create_classified_sample_from_NN(clf, get_eq_sample_size(len(V)), V)
#     return dataset  # , I


def custom_MQ(antecedent,heads, NN,n_variables):
    X = np.empty((len(heads),n_variables*3))
    for i,h in enumerate(heads):
        assignment = np.array([1 if v in antecedent else -1 for v in range(n_variables) ])
        assignment[h] = 0
        X[i,:] = to_categorical(assignment,num_classes=3).reshape(n_variables*3)
    return (NN.predict(X,verbose = 0) > 0.5).astype("int32").flatten()


def extract_horn_with_queries_1(clf, n_variables, iterations):
    bad_ne = []

    mq = lambda a,c: custom_MQ(a,c, clf,n_variables)
    memory = [clf, n_variables, bad_ne, 0]
    eq = lambda hypothesis,i: custom_EQ(hypothesis,i,memory)

    start = timeit.default_timer()
    horn = LRN(mq,n_variables)
    ith=1
    terminate,answer = eq(horn.hypothesis,ith)
    while not terminate and ith < iterations:
        print('\r{}\{}   '.format(ith,iterations),end='')
        horn.process_counterexample(answer)
        ith+=1
        terminate,answer = eq(horn.hypothesis,ith)

    stop = timeit.default_timer()
    runtime = stop - start

    runtime_per_iteration = runtime / iterations
    return (horn.hypothesis, runtime, runtime_per_iteration, memory[3])




def generate_partial_truth_table(dataset, labelling_function):
    r = []
    for assignment in dataset:
        v = labelling_function(assignment)
        r.append(v)
    return r


def obtain_absolute_differences_horn(T, clf, H,inctree, n_variables):
    # m = 2 * int(get_eq_sample_size(n_variables))
    m = SIZE_DIFFERENCES_TEST
    #     m = pow(2,len(V))
    max_n = pow(2, n_variables) - 1
    distribution = uniform_distribution(max_n, n_variables)
    dataset = []
    for i in range(m):
        assignment = next(distribution)
        dataset.append(assignment)

    labelling_horn = lambda assignment: satisfiable(T, assignment)
    tt_t = generate_partial_truth_table(dataset, labelling_horn)

    labelling_horn = lambda assignment: satisfiable(H, assignment)
    tt_h = generate_partial_truth_table(dataset, labelling_horn)

    def labelling_NN(assignment):
        a = to_categorical(assignment,num_classes=3)
        a = a.reshape(n_variables*3)
        return (clf.predict(np.array([a]),verbose = 0)[0] > 0.5).astype("int32")
    tt_nn = generate_partial_truth_table(dataset, labelling_NN)

    #    labelling_tree = lambda assignment: 1 if chef.predict(tree,assignment) == 'Yes' else 0
    #    tt_tree = generate_partial_truth_table(dataset,labelling_tree,V,m)

    labelling_inctree = lambda assignment: inctree.predict([assignment])[0]
    tt_inctree = generate_partial_truth_table(dataset,labelling_inctree)

    diff_t_h = 0
    for i in range(m):
        diff_t_h = diff_t_h + abs(tt_t[i] - tt_h[i])
    #     print('The cumulative difference between the possibility value of each '
    #           'interpretation in the truth table of the hypothesis and the '
    #           'target is {}\n\n'.format(diff_t_h))

    diff_t_nn = 0
    for i in range(m):
        diff_t_nn = diff_t_nn + abs(tt_t[i] - tt_nn[i])
    #     print('The cumulative difference between the possibility value of each '
    #           'interpretation in the truth table of the target and the '
    #           'neural network is {}\n\n'.format(diff_t_nn))

    diff_h_nn = 0
    for i in range(m):
        diff_h_nn = diff_h_nn + abs(tt_h[i] - tt_nn[i])
    #     print('The cumulative difference between the possibility value of each '
    #           'interpretation in the truth table of the hypothesis and the '
    #           'neural network is {}\n\n'.format(diff_h_nn))

    diff_t_tree = 0
    #    for i in range(m):
    #        diff_t_tree = diff_t_tree +abs(tt_tree[i] - tt_t[i])

    diff_t_inctree = 0
    for i in range(m):
        diff_t_inctree = diff_t_inctree +abs(tt_inctree[i] - tt_t[i])

    a = m
    return (diff_t_h / a, diff_t_nn / a, diff_h_nn / a, diff_t_tree / a, diff_t_inctree / a)


def make_experiment(noise_list, variables_list, extract_function, extract_tree):
    for noise in noise_list:
        print('Results with noise {} :'.format(noise / 100))
        xp = []
        for n_variables in variables_list:
            #         print('iteration {}  '.format(n_variables),end='\r')
            xp.append(n_variables)
            # Var = define_variables(n_variables)
            avg_diff_th = 0
            avg_diff_hnn = 0
            avg_diff_tnn = 0
            avg_diff_t_tree = 0
            avg_diff_t_inctree = 0
            avg_time_learning = 0
            avg_time_inctree = 0
            avg_acc = 0
            avgcalls = 0
            iters=ITERATIONS
            for i in range(iters):
                print('generating target          ', end='\r')
                T = generate_target(n_variables, randint(int(n_variables / 2),n_variables))
                #                 print(T)
                size_training_sample = trainingsize(n_variables)
                print('generating training dataset', end='\r')
                dataset = create_classified_sample_randomly(T,
                                                            size_training_sample,
                                                            n_variables, noise=noise)
                X_data = to_categorical(dataset[:, :-1], num_classes=3)
                X_data = X_data.reshape((X_data.shape[0],X_data.shape[1]*3))
                y_data = dataset[:,-1:]
                # Split data into train and testing data
                X_train, X_test, y_train, y_test = train_test_split(X_data, y_data, test_size=0.3, shuffle=True)
                clf = nn(n_variables*3)

                print('training NN                 ', end='\r')
                callback = EarlyStopping(monitor='loss', patience=3,min_delta=0.05,verbose=0)
                start = timeit.default_timer()
                history = clf.fit(X_train, y_train,
                                  validation_data=(X_test, y_test),
                                  callbacks=[callback],
                                  epochs=EPOCHS, batch_size=BATCH_SIZE, verbose=0)
                print('Training NN time: {}        '.format(timeit.default_timer() - start))

                print('training tree                  ', end='\r')
                # tree = train_tree(X_train, y_train)

                print('Extracting HORN            ', end='\r')
                (h, runtime, runtime_per_iteration, calls) = extract_function(clf, n_variables)
                #                 print('Hypothesis: ',h)

                print(f'target: {T}\nhypothesis: {h}')

                print('training inc_tree        ', end='\r')
                (inctree, tree_runtime) = extract_tree(clf, n_variables)
                # (inctree, tree_runtime) = 0,0

                print('counting differences        ', end='\r')
                (diff_t_h, diff_t_nn, diff_h_nn, diff_t_tree, diff_t_inctree) = obtain_absolute_differences_horn(T, clf,
                                                                                                                 h,
                                                                                                                 inctree,
                                                                                                                 n_variables)

                avg_diff_th += diff_t_h
                avg_diff_hnn += diff_h_nn
                avg_diff_tnn += diff_t_nn
                avg_diff_t_tree += diff_t_tree
                avg_diff_t_inctree += diff_t_inctree
                avg_time_learning += runtime
                avg_time_inctree += tree_runtime
                avg_acc += history.history['val_accuracy'][-1]
                avgcalls = calls

            print('                                ', end='\r')
            print()
            print(("#Variables: {}\n" +
                   "diff_t_h: {}\n" +
                   "diff_t_nn: {}\n" +
                   "diff_h_nn: {}\n" +
                   "diff_t_tree: {}\n" +
                   "diff_t_inctree: {}\n" +
                   "Learning Time: {}\n" +
                   "Learning Time inctree: {}\n" +
                   "NN loss: {}\n#NN calls: {}\n").format(
                n_variables,
                avg_diff_th / iters,
                avg_diff_tnn / iters,
                avg_diff_hnn / iters,
                avg_diff_t_tree / iters,
                avg_diff_t_inctree / iters,
                avg_time_learning / iters,
                avg_time_inctree / iters,
                avg_acc / iters,
                (avgcalls / iters)
            ))
            print()

n_eq = MAX_EQ_QUERIES
f1 = lambda clf,n_variables: extract_horn_with_queries_1(clf,n_variables, n_eq)
extract_tree = lambda clf,n_variables: inc_train_tree(clf,n_eq,n_variables)
make_experiment([10],[100],f1,extract_tree)
#
# a = LRN()

