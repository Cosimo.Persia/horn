# class Partial_Interpretation:
#     def __init__(self,pos,neg):
#         #sets
#         self.pos = pos
#         self.neg = neg
import copy

class LRN:
    def __init__(self,MQ,variable_n):
        self.variable_n = variable_n
        self.hypothesis = [] #list of pair
        self.S = [] #list of sets of antecedents
        self.MQ = MQ #function of 2 arguments

    def process_counterexample(self,positive_var):
        alpha_hat = self.unit_propagation(positive_var)
        replaced = False
        for ix, alpha in enumerate(self.S):
            intersection = alpha.intersection(alpha_hat)
            if len(intersection)>0 and (intersection < alpha) and  len(self.RHS(intersection))>0:
                self.S[ix] = intersection
                replaced = True
                break
        if not replaced:
            self.S.append(alpha_hat)


        self.hypothesis = []
        for idx,alpha in enumerate(self.S):
            heads = self.RHS(alpha)
            if len(heads) == 0:
                self.S.pop(idx)
            else:
                for h in heads:
                    self.hypothesis.append([alpha, {h}])
    def unit_propagation(self, set_unit_clauses):
        rules = copy.deepcopy(self.hypothesis)
        forcefully_true = set()
        for c in set_unit_clauses:
            a = set()
            a.add(c)
            rules.append([set(),a])
        no_propagation = False
        while not no_propagation:
            no_propagation = True
            for ix,(ant,cons) in enumerate(rules):
                if len(ant) > 0:
                    for v in forcefully_true:
                        if v in ant:
                            rules[ix][0].remove(v)
                            no_propagation = False
                if len(ant) == 0:
                    if len(cons) == 0:
                        raise Exception('cons is empty, incosistent rules')
                    if list(cons)[0] not in forcefully_true:
                        forcefully_true.add(list(cons)[0])
                        no_propagation = False

                # if len(ant) > 0:
                #     all_unit_clauses = False
        return forcefully_true

    def RHS(self,antecedent):
        heads = set()
        candidates = set(range(self.variable_n))
        # candidates = self.unit_propagation(antecedent).difference(antecedent)
        candidates_list = list(candidates - (antecedent))
        if len(candidates_list) > 0:
            answer = self.MQ(antecedent,candidates_list)
            for i,c in enumerate(candidates_list):
                if answer[i] ==0:
                    heads.add(c)
        return heads

